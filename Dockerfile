FROM python:3.7.9-alpine3.12

ADD remarkableFeeds .
ADD requirements.txt .
RUN pip install -r requirements.txt