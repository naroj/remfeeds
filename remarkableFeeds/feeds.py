"""
Feeds module
"""

from typing import List
import re
# import logging
# import requests
import feedparser


# from tempfile import NamedTemporaryFile
# from weasyprint import HTML

__all__ = ["fetch_feeds"]


def fetch_feeds(feeds: List[str]) -> dict:
    """ cycle feed urls through parser (fetch remote content) """

    def fetch_feed(feed: feedparser.util.FeedParserDict) -> tuple:
        parsed = feedparser.parse(feed)
        dir_name = re.sub(r"\W+", "_", parsed.feed.title)
        entries = fetch_entries(parsed.entries)
        return dir_name, entries

    my_feeds = [fetch_feed(feed) for feed in feeds]
    return {item[0]: item[1] for item in my_feeds}


def fetch_entries(entries: List[dict]) -> List[dict]:
    """ fetch entry article from link """
    article_limit = 5
    max_title_len = 30
    
    def validate(entry: dict):
        expected = {"link", "published_parsed"}
        if expected.issubset(entry.keys()):
            return True
        else:
            return False     

    def fetch(entry: dict) -> dict:
        clean_title = re.sub(r"\W+", "_", entry["title"])
        if len(clean_title) > max_title_len:
            clean_title = clean_title[:max_title_len]
        return {
            "file_name": f"{clean_title}.pdf",
            "link": entry["link"],
            "published_parsed": entry["published_parsed"],
            "article": None,
        }

    fetched = [fetch(entry) for entry in entries if validate(entry)]
    sort = sorted(fetched, reverse=True, key=lambda k: k["published_parsed"])
    return sort[:article_limit]
