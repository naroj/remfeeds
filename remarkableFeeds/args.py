"""
CLI module
"""

import sys
import argparse
import logging
import string
from validator_collection import checkers

__all__ = ["parse_params"]


DESCR = """
remarkable cloud RSS client. In order to use this tool you need to register your device. This can be done via https://my.remarkable.com/connect/remarkable. The registration process returns a device and user token. Those are long-lived (eternal?) tokens you can pass to this tool in order to run things without human interaction.
"""


def sanitize_input(argvs: list) -> list:
    """ sanitize sys.argv """
    feeds, tokens, errors = [], [], []
    special_chars = [c for c in string.punctuation if c != "."]
    for num, item in enumerate(argvs):
        if item == "--feed":
            feeds.append(argvs[num + 1])
        elif item in ("--device-token", "--user-token", "--register-token"):
            tokens.append(argvs[num + 1])
    for feed in feeds:
        if not checkers.is_url(feed):
            errors.append(f"invalid feed: {feed}")
    for token in tokens:
        chars = [c for c in token]
        hit = [char for char in chars if char in special_chars]
        if hit:
            errors.append(f"token {token} contains invalid characters: {hit}")
    if errors:
        for err in errors:
            logging.error(err)
        sys.exit(1)
    return argvs


def parse_params(args: list) -> dict:
    """ collect CLI parameters """
    parser = argparse.ArgumentParser(description=DESCR)
    parser.add_argument(
        "--device-token", required=False, default=None, help="Device token"
    )
    parser.add_argument("--user-token", required=False, default=None, help="User token")
    parser.add_argument(
        "--register-token", required=False, default=None, help="Register token"
    )
    parser.add_argument(
        "--show-example-conf",
        required=False,
        action="store_true",
        help="show an example config",
    )
    parser.add_argument(
        "--feed",
        required=False,
        action="append",
        help="RSS feed (may be used multiple times) NOTE: http{s} scheme is required!",
    )
    parser.add_argument(
        "--cloud-path", required=False, help="path in cloud storage to store feeds in"
    )
    parser.add_argument(
        "--conf", required=False, help="TODO: Path to configuration file"
    )
    cli_args = parser.parse_args(args)
    return cli_args.__dict__
