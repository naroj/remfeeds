"""
Identify with RM cloud using available mechanisms
"""

import sys
import logging
from typing import Union
from rmapy import exceptions as exc
from rmapy.api import Client
from rmapy.document import ZipDocument
from rmapy.document import Document
from rmapy.folder import Folder
from rmapy.meta import Meta


def auth_client(device_token: str, user_token: str) -> Client:
    """ authenticate this client based on valid tokens """
    token_set = {"devicetoken": device_token, "usertoken": user_token}
    auth_api = Client()
    auth_api.token_set = token_set
    auth_api.renew_token()
    return auth_api


def register_client(register_token: str) -> Union[dict, bool]:
    """
    register this client with a code generated from;
    https://my.remarkable.com/connect/remarkable
    """
    if not register_token:
        logging.error("can not register client, REGISTER_TOKEN ENV var is not set")
        return False
    reg_api = Client()
    try:
        reg_api.register_device(register_token)
        reg_api.renew_token()
    except exc.AuthError as auth_error:
        logging.error("API returned and authentication error: %s", auth_error)
        return False
    return reg_api.token_set


def get(
    rm_api: Client,
    parent_id: str = "",
    unique_name: str = "",
    kind: str = "meta",
) -> list:
    """ get document/folder or both objects optional by parent name """
    available = {
        "folder": ("CollectionType"),
        "document": ("DocumentType"),
        "meta": ("CollectionType", "DocumentType"),
    }
    selected = available.get(kind)
    if not selected:
        logging.error(
            "unexpected kind requested: %s, choose one of %s" % (kind,
            tuple(available.keys())),
        )
        return []
    collection = rm_api.get_meta_items()
    if parent_id:
        return [
            item
            for item in collection
            if item.Type in selected and item.Parent == parent_id
        ]
    return [item for item in collection if item.Type in selected]


def upload_doc_to_folder(rm_api: Client, doc_file: str, folder_name: str) -> bool:
    """ upload a document (local path to pdf/epub) to a remote folder by name """
    # TODO add support for parent specific folder
    zip_doc = ZipDocument(doc=doc_file)
    dest = [i for i in rm_api.get_meta_items() if i.VissibleName == folder_name][0]
    result = rm_api.upload(zip_doc, dest)
    return result


def delete_document(rm_api: Client, doc: Document) -> bool:
    """ delete an item, doc or folder """
    #TODO: this is broken, check docs and fix
    remove = rm_api.delete(doc)
    if remove:
        logging.info("removed document %s, ID: %s", doc.VissibleName, doc.ID)
    else:
        logging.error("removal of document %s failed, ID: %s", doc.VissibleName, doc.ID)
    return remove


"""
if __name__ == "__main__":
    RM_API = cloud_identify()
    docs = get_objects(rm_api=RM_API, parent="Feeds", kind=Document)
    # delete_doc(docs[0])
"""
