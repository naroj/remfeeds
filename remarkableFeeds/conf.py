"""
config logic
"""

import os
import sys
from typing import Union
import logging
import configparser

HOME = os.getenv("HOME")
DEFAULT_FILE = ".remarkableFeeds.ini"


def show_example(
    stdout: bool = False,
    register_token: str = "",
    device_token: str = "",
    user_token: str = "",
    cloud_path: str = "",
) -> configparser.ConfigParser:
    """ show example config content """
    config = configparser.ConfigParser()
    urls = "http://feed1.example.com\nhttps://feed2.example.com"
    config["remarkable"] = {
        "devicetoken": f"{device_token}",
        "usertoken": f"{user_token}",
        "cloudpath": f"{cloud_path}",
    }
    config["feeds"] = {"urls": urls}
    if stdout:
        config.write(sys.stdout)
    return config


def validate(conf: configparser.ConfigParser) -> bool:
    """ validate config """
    # TODO add token/option combination checks
    errors = []
    expected_struct = {
        "remarkable": ["devicetoken", "usertoken", "cloudpath"],
        "feeds": [],
    }
    for exp_section, exp_options in expected_struct.items():
        if exp_section not in conf.sections():
            errors.append(f"missing config section {exp_section}")
        if errors:
            break
        for option in exp_options:
            if not conf.has_option(exp_section, option):
                errors.append(f"missing option {option} in section {exp_section}")
    if not errors:
        logging.debug("config validation succeeded")
        return True
    for err in errors:
        logging.error(err)
    return False


def read_ini(path: str) -> Union[configparser.ConfigParser, bool]:
    """ read existing config file """
    config = configparser.ConfigParser()
    config.read(path)
    if validate(config):
        return config
    return False


if __name__ == "__main__":
    MY_FILE = "/home/amsterdam/.remarkableFeeds.ini"
    MY_CONF = read_ini(path=MY_FILE)
