"""
Test CLI parameter interpretation logic
"""

import sys
import pytest
from remarkableFeeds import args


def test_input_sanitizer_tokens_valid():
    argvs = [
        "--device-token", "i.am.an.acceptable.token", 
        "--user-token", "i.am.a.very.acceptable.token",
        "--register-token", "i.am.an.exceptionally.acceptable.token"
    ]
    sanitize = args.sanitize_input(argvs)
    assert "i.am.an.acceptable.token" in sanitize


def test_input_sanitizer_tokens_invalid():
    argvs = [
        "--device-token", "i.am<an.unacceptable.token", 
        "--user-token", "i.am.a.*very.unacceptable.token",
        "--register-token", "i.am.an.excepti%^$^onally.unacceptable.token"
    ]
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        args.sanitize_input(argvs)
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 1


def test_input_sanitizer_feeds_valid():
    argvs = [
        "--feed", "https://example.nl",
        "--feed", "http://example.com"
    ]
    sanitize = args.sanitize_input(argvs)
    assert "https://example.nl" in sanitize

def test_input_sanitizer_feeds_invalid():
    argvs = [
        "--feed", "example.nl",
        "--feed", "ht://example.com"
    ]
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        args.sanitize_input(argvs)
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 1

"""
def test_user_dev_failing(caplog):
    params = {"device_token": "XXXXX", "user_token": ""}
    verify = args.verify_params(params)
    expected_error = "both the user and device token need to be specified or neither"
    assert verify == False
    assert expected_error in caplog.text
"""

"""
def test_user_dev_token_working():
    params = {"device_token": "XXXXX", "user_token": "XXX", "register_token": None}
    verify = args.verify_params(params)
    assert verify == True
"""

"""
def test_register_token_failing():
    params_dev = {"device_token": "xx", "user_token": None, "register_token": "abc"}
    verify_dev = args.verify_params(params_dev)
    assert verify_dev == False
    params_dev_user = {
        "user_token": "yy",
        "device_token": "yy",
        "register_token": "abc"
    }
    verify_dev_user = args.verify_params(params_dev_user)
    assert verify_dev_user == False
"""


"""
def test_register_token_working():
    params = {"device_token": None, "user_token": None, "register_token": "abc"}
    verify = args.verify_params(params)
    assert verify == True
"""

def test_args_argparse():
    """ go through all the possible switches """
    switches = ("--register-token", "--user-token", "--device-token", "--conf")
    opt_value = "beautiful_weather_today"
    for switch in switches:
        test_switch = [switch, opt_value]
        parser = args.parse_params(args=test_switch)
        parser_switch = switch[2:].replace("-", "_")
        assert parser_switch in parser.keys()
        assert parser[parser_switch] == opt_value
