"""
Test the remarkable client API module
"""

import pytest_mock
import pytest
from remarkableFeeds import cloudapi


class ZipDocument:

    def __init__(self, doc: str):
        self.doc = doc



class Meta:

    VissibleName = ""
    Parent = None
    Type = "DocumentType"

    def __init__(self, v_name):
        self.VissibleName = v_name



class Client:

    def __init__(self):
        self.items = [Meta("apple"), Meta("kiwi")]

    def get_meta_items(self) -> list:
        return(self.items)

    def upload(self, zip_doc, dest) -> list:
        return [dest]

    def delete(self, doc):
        pass
        

def test_auth_client(mocker):
    """ test the authentication routine """
    tokens = {"device_token": "DD", "user_token": "TT"}
    mocker.patch.object(cloudapi, 'Client')
    cloudapi.Client.renew_token.return_value = True
    auth = cloudapi.auth_client(**tokens)
    assert auth.token_set["devicetoken"] == tokens["device_token"]
    assert auth.token_set["usertoken"] == tokens["user_token"]


def test_register_client_fail(caplog):
    """ failing register client with empty token """
    reg = cloudapi.register_client(register_token="")
    assert "can not register client" in caplog.text
    assert reg == False


def test_register_client_success(mocker):
    """ remote registration of our client """
    mocker.patch.object(cloudapi, 'Client')
    cloudapi.Client.renew_token.return_value = True
    cloudapi.Client.register_device.return_value = None
    reg = cloudapi.register_client(register_token="XXX")
    assert reg != False


def test_get_fail(caplog):
    try_in_vain = cloudapi.get(rm_api=Client(), kind="wrong_kind")
    assert "unexpected kind requested" in caplog.text
    assert try_in_vain == []


def test_get_parent(caplog):
    client = Client()
    old_book = Meta("old_book")
    mock_parent_id = "f7b78476-7525-49b6-9817-b7d864abb948"
    old_book.Parent = mock_parent_id
    client.items.append(old_book)
    get = cloudapi.get(rm_api=client, parent_id=mock_parent_id)
    assert get[0].VissibleName == "old_book"
    
    
def test_get_type_selection(caplog):
    client = Client()
    magazine = Meta("Magazine")
    magazine.Type = "DocumentType"
    my_folder = Meta("My Folder")
    my_folder.Type = "CollectionType"
    client.items.extend([magazine, my_folder])
    get_folder = cloudapi.get(rm_api=client, kind="folder")
    get_doc = cloudapi.get(rm_api=client, kind="document")
    get_all = cloudapi.get(rm_api=client)
    assert get_folder[0].VissibleName == "My Folder"
    assert "Magazine" in [d.VissibleName for d in get_doc]
    assert "My Folder" not in [d.VissibleName for d in get_doc]
    assert "My Folder" in [f.VissibleName for f in get_folder]
    assert "Magazine" not in [f.VissibleName for f in get_folder]
    # assert get_doc[0].VissibleName == "Magazine"
    # assert 1 == [i.Type for i in get_all]


def test_upload_docs(mocker):
    """ test if an uploaded doc ends up in the folder we point it to """
    dest_folder = "kiwi"
    awesome_api = Client()
    cloudapi.ZipDocument = ZipDocument
    result = cloudapi.upload_doc_to_folder(
        rm_api=awesome_api, 
        folder_name=dest_folder, 
        doc_file="very_sensitive_secrets.pdf"
    )
    assert result[0].VissibleName == dest_folder
