import sys
import pytest
import pytest_mock
from remarkableFeeds import cli


def test_parse(capsys, mocker):
    test_argvs = ["script.py", "--show-example-conf"]
    mocker.patch.object(sys, "argv", test_argvs)
    out, err = capsys.readouterr()
    params = cli.parse_argvs()
    assert "show_example_conf" in params.keys()
    assert params["show_example_conf"] == True



"""
def test_feeds_handler(mocker):
    test_dir_name = "yolo_dir"
    fake_data = [{"raw_entries" : [{"entry": "happy entry"}], "metadata": {"dir_name": test_dir_name}}]
    mocker.patch("remarkableFeeds.feeds.fetch_feeds", return_value=fake_data)
    filtered_entries = cli.feeds_handler([])
    assert test_dir_name in filtered_entries.keys()
"""
