""" test conf """

import logging
import sys
import pytest
import configparser
from io import StringIO
from remarkableFeeds import conf


def test_example():
    parser = conf.show_example(stdout=False)
    assert "remarkable" in parser.sections()
    assert "feeds" in parser.sections()

def test_show_example_stdout(capsys):
    conf.show_example(stdout=True)
    out, err = capsys.readouterr()
    parser = configparser.ConfigParser(allow_no_value=True)
    parser.read_file(StringIO(out))
    assert "remarkable" in parser.sections()
    assert "feeds" in parser.sections()



def test_parsing_missing_section(caplog):
    parser = conf.show_example()
    parser.remove_section("remarkable")
    validate = conf.validate(parser)
    assert validate == False
    assert "missing config section remarkable" in caplog.text


def test_parsing_missing_option(caplog):
    parser = conf.show_example()
    parser.remove_option("remarkable", "usertoken")
    conf.validate(parser)
    assert "missing option usertoken" in caplog.text


def test_read_ini_file_success():
    parser = conf.show_example()
    test_file = "/tmp/test.ini"
    with open(test_file, "w") as fp:
        parser.write(fp)
    reader = conf.read_ini(test_file)
    assert isinstance(reader, configparser.ConfigParser) == True


def test_read_ini_file_fail():
    test_file = "/no/file/here"
    reader = conf.read_ini(test_file)
    assert reader == False
