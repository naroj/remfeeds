"""
tests for the papersync feeds module
"""

import logging
import pytest
import requests
import requests_mock

# import weasyprint
import time
from remarkableFeeds import feeds

LOGGER = logging.getLogger(__name__)


class SubFeed:
    pass


class TestFeed:
    status = 200
    feed = SubFeed()
    entries = []


class TestFeedBad:
    status = 200


class FakeWeasy:
    def __init__(self, link):
        self.link = link

    def write_pdf(self):
        return "whoopie"


def test_fetch_feeds(mocker):
    feed_title = "happy feed"
    entry_link = "http://example.nl"
    entry_title = "nice article"
    entry_published = time.localtime(1545925769)
    feed_object = TestFeed()
    feed_object.entries.append(
        {"title": entry_title, "link": entry_link, "published_parsed": entry_published}
    )
    feed_object.feed.title = feed_title
    important_feeds = ["https://example.org"]
    mocker.patch("feedparser.parse", return_value=feed_object)
    my_feeds_folder = feed_title.replace(" ", "_")
    my_article_filename = "%s.pdf" % entry_title.replace(" ", "_")
    my_feeds = feeds.fetch_feeds(important_feeds)
    my_test_entry = my_feeds[my_feeds_folder][0]
    assert my_feeds_folder in my_feeds.keys()
    assert my_test_entry["file_name"] == my_article_filename
    assert my_test_entry["published_parsed"].tm_year == 2018
    assert my_test_entry["published_parsed"].tm_mon == 12
    assert my_test_entry["article"] is None
    """
    expect_title = feed_title.replace(" ", "_")
    assert expect_title in my_feeds.keys()
    #assert my_feeds[expect_title][0]["file_name"].endswith(".pdf") == True
    # assert my_feeds[expect_title][0]["link"] == entry_link
    """


def test_fetch_entries_special_conditions():
    publish_time = time.localtime(1545925769)
    max_title_len = 30
    my_entries = [
        {"title": "my title", "published_parsed": publish_time}
    ]
    result_no_link = feeds.fetch_entries(my_entries)
    my_entries[0]["title"] = "my very very very very very long article title"
    my_entries[0]["link"] = "https://example.nl/feed/article"
    result_long_title = feeds.fetch_entries(my_entries)
    del(my_entries[0]["published_parsed"])
    result_no_pub_date = feeds.fetch_entries(my_entries)
    assert len(result_no_pub_date) == 0
    assert len(result_no_link) == 0
    assert len(result_long_title[0]["file_name"]) == max_title_len + 4


def test_entries_sorting_by_date():
    """ make sure ordering by date is working """
    test_entries = [
        {
            "link": "http://example.org",
            "published_parsed": time.localtime(6145925769),
            "title": "Whales can not fly",
        },
        {
            "link": "http://example.io",
            "published_parsed": time.localtime(1111925769),
            "title": "dogs have hair",
        },
        {
            "link": "http://example.io",
            "published_parsed": time.localtime(1445925769),
            "title": "birds have feathers",
        },
    ]
    date_order = {}
    result = feeds.fetch_entries(test_entries)
    entry_keys = ("file_name", "link", "published_parsed", "article")
    for num, entry in enumerate(result):
        date_order[num] = entry["published_parsed"]
        if num < 1:
            continue
        assert date_order[num - 1] > date_order[num]
        assert set(entry_keys).issubset(entry.keys())
        assert entry["file_name"].endswith(".pdf") == True


def test_fetch_article(mocker):
    # weazy = FakeWeasy("sdad")
    # mocker.patch("weasyprint.HTML", return_value=weazy)
    # article = feeds.fetch_article("example.nl")
    # assert isinstance(article.read(), bytes)
    # assert article.close() is None
    assert None is None
