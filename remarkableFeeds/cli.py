"""
Module for running this awesome remarkable cloud RSS syncer from the commandline
I think this will be the primary interface since we'll run this from cloud functions or in a K8S-like environment
"""

import sys
import logging
import pprint
import time
from . import (
    args,
    conf,
    cloudapi,
    feeds
)


def parse_argvs() -> dict:
    """ parse CLI parameters """
    if not sys.argv[1:]:
        logging.error("no arguments means no action, use --help to find out more")
        sys.exit(1)
    safe_input = args.sanitize_input(sys.argv[1:])
    params = args.parse_params(args=safe_input)
    return params


def option_router(params: dict):
    """ route options to app functionality """
    if params["show_example_conf"]:
        conf.show_example(stdout=True)
    elif params["register_token"]:
        tokens = cloudapi.register_client(params["register_token"])
        if not tokens:
            sys.exit(1)
        conf.show_example(
            stdout=True,
            device_token=tokens["devicetoken"],
            user_token=tokens["usertoken"],
        )
    elif all([params["user_token"], params["device_token"], params["feed"]]):
        """ validate input and activate the feed_handler """
        logging.debug("option router sends request to feed_handler")
        feeds = feeds_handler(params)


def feeds_handler(params: dict) -> dict:
    """ execute feed logic, next: sync results with the cloudapi """
    rm_api = cloudapi.auth_client(params["device_token"], params["user_token"])
    rss_data = feeds.fetch_feeds(params["feed"])
    #y = cloudapi.get(rm_api, kind="folder", parent_id="059c452e-33dd-4441-a941-a32a249d097a")
    y = cloudapi.get(rm_api, kind="document")
    print([f.Type for f in y])
    # remote_cloud_path = cloudapi.get
    # feeds_folders(folders=list(rss_data.keys()))

    
    
def feeds_folders(folders: list):
    print(folders)


def entries_handler(feeds_data: dict):
    """ fetch entries as pdf files delivered as tmp files """
    pass


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    params = parse_argvs()
    option_router(params)
