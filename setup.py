from setuptools import setup, find_packages
setup(
    name='remarkableFeeds',
    version='2.1',
    author="Joran Franx",
    author_email="bucket+remarkablefeeds@kernelbug.org",
    description="Get remote content to your remarkable",
    url="https://127.0.0.1",
    include_package_data=True,
    install_requires=[
        'requests',
        'feedparser',
        'html2text',
        'pdfkit',
        'pyYAML',
        'rmapy',
        'validator-collection',
        'fpdf',
    ],
    packages=find_packages()
)
